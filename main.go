package main

import "fmt"
import "database/sql"
import _ "github.com/go-sql-driver/mysql"
import "net/http"
import "strings"
import "github.com/kelvins/geocoder/structs"
import "errors"
import "strconv"
import "encoding/json"

const (
	geocodeApiUrl = "https://maps.googleapis.com/maps/api/geocode/json?"
)

var ApiKey string

// Address structure used in the Geocoding and GeocodingReverse functions
// Note: The FormattedAddress field should be used only for the GeocodingReverse
// to get the formatted address from the Google Geocoding API. It is not used in
// the Geocoding function.
type Address struct {
	Street           string
	Number           int
	Neighborhood     string
	District         string
	City             string
	County           string
	State            string
	Country          string
	PostalCode       string
	FormattedAddress string
	Types            string
}

// Location structure used in the Geocoding and GeocodingReverse functions
type Location struct {
	Latitude  float64
	Longitude float64
}

func connect() (*sql.DB, error) {
    db, err := sql.Open("mysql", "root:root@tcp(127.0.0.1:3306)/fds_db")
    if err != nil {
        return nil, err
    }

    return db, nil
}

func main() {
  var db, err = connect()
  if err != nil {
      fmt.Println(err.Error())
  }
  defer db.Close()

  // Execute the query
  results, err := db.Query("select idrecordatm,lokasi, alamat from tbl_atmlocation where longitude = '' limit 10000;")
  if err != nil {
    panic(err.Error()) // proper error handling instead of panic in your app
  }

  defer db.Close()
  idrecordatm := ""
  lokasiatm := ""
  alamat := ""
  ApiKey = "AIzaSyCO61_FHmrUaK1eGlcLwR8VKFCEkaEkelE"
  for results.Next() {
    err = results.Scan(&idrecordatm, &lokasiatm, &alamat)
    if err != nil {
      panic("Error Lho 1 " + err.Error()) // proper error handling instead of panic in your app
    }

    address := lokasiatm //+ "," + alamat
    address = strings.Replace(address, " ", "+", -1)

    // Create the URL based on the formated address
  	url := geocodeApiUrl + "address=" + address

  	// Use the API Key if it was set
  	if ApiKey != "" {
  		url += "&key=" + ApiKey
  	}
    fmt.Println("URL :"+url)
    // Send the HTTP request and get the results
  	resultsReq, err := httpRequest(url)
  	if err != nil {
  		fmt.Println("Error Lho 2 " + err.Error())
  	}

    if strings.ToUpper(resultsReq.Status) != "OK" {
      fmt.Println("ID Record ATM :"+idrecordatm+" Not Found")
    } else {
      latitude := resultsReq.Results[0].Geometry.Location.Lat
      longitude := resultsReq.Results[0].Geometry.Location.Lng

      latitudeStr := strconv.FormatFloat(latitude, 'f', 6, 64)
      longitudeStr := strconv.FormatFloat(longitude, 'f', 6, 64)
      _, errDBUpdate := db.Query("update tbl_atmlocation set longitude = '"+longitudeStr+"', latitude = '"+latitudeStr+"' where idrecordatm = "+idrecordatm)

      if errDBUpdate != nil {
    		fmt.Println(err.Error())
    	}
      defer db.Close()
    }
  }
}

// httpRequest function send the HTTP request, decode the JSON
// and return a Results structure
func httpRequest(url string) (structs.Results, error) {

	var results structs.Results

	// Build the request
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return results, err
	}

	// For control over HTTP client headers, redirect policy, and other settings, create a Client
	// A Client is an HTTP client
	client := &http.Client{}

	// Send the request via a client
	// Do sends an HTTP request and returns an HTTP response
	resp, err := client.Do(req)
	if err != nil {
		return results, err
	}

	// Callers should close resp.Body when done reading from it
	// Defer the closing of the body
	defer resp.Body.Close()

	// Use json.Decode for reading streams of JSON data
	err = json.NewDecoder(resp.Body).Decode(&results)
	if err != nil {
		return results, err
	}

	// The "OK" status indicates that no error has occurred, it means
	// the address was analyzed and at least one geographic code was returned
	if strings.ToUpper(results.Status) != "OK" {
		// If the status is not "OK" check what status was returned
		switch strings.ToUpper(results.Status) {
		case "ZERO_RESULTS":
			err = errors.New("No results found.")
			break
		case "OVER_QUERY_LIMIT":
			err = errors.New("You are over your quota.")
			break
		case "REQUEST_DENIED":
			err = errors.New("Your request was denied.")
			break
		case "INVALID_REQUEST":
			err = errors.New("Probably the query is missing.")
			break
		case "UNKNOWN_ERROR":
			err = errors.New("Server error. Please, try again.")
			break
		default:
			break
		}
	}

	return results, err
}
